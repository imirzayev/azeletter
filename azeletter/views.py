from django.shortcuts import render
from django.core.files.base import ContentFile
from django.core.files.storage import FileSystemStorage
import base64
import pathlib, os
from django.conf import settings
from tensorflow.keras.models import save_model, load_model
import cv2
import numpy as np


def add_data(request):
    if request.method == 'POST':
        myfile = request.POST.get('image')
        format, imgstr = myfile.split(';base64,')
        ext = format.split('/')[-1]
        label = request.POST.get('label')
        location = settings.MEDIA_ROOT + '/' + label
        pathlib.Path(location).mkdir(parents=True, exist_ok=True)

        count = len(os.listdir(location))

        data = ContentFile(base64.b64decode(imgstr), name=str(count + 1) + '.' + ext)

        fs = FileSystemStorage(location=location)
        filename = fs.save(data.name, data)
        uploaded_file_url = fs.url(filename)
        return render(request, 'add_data.html', {
            'uploaded_file_url': uploaded_file_url
        })
    return render(request, 'add_data.html')

def add_word(request):
    if request.method == 'POST':
        myfile = request.POST.get('image')
        format, imgstr = myfile.split(';base64,')
        ext = format.split('/')[-1]
        label = request.POST.get('label')
        location = settings.MEDIA_ROOT + '/words'
        pathlib.Path(location).mkdir(parents=True, exist_ok=True)

        count = len(os.listdir(location))

        data = ContentFile(base64.b64decode(imgstr), name=label + '.' + ext)

        fs = FileSystemStorage(location=location)
        filename = fs.save(data.name, data)
        uploaded_file_url = fs.url(filename)
        return render(request, 'add-word.html', {
            'uploaded_file_url': uploaded_file_url
        })
    return render(request, 'add-word.html')

def index(request):
    pic_size = 60
    if request.method == 'POST':
        myfile = request.POST.get('image')
        format, imgstr = myfile.split(';base64,')
        ext = format.split('/')[-1]
        location = settings.MEDIA_ROOT + '/predictions'
        pathlib.Path(location).mkdir(parents=True, exist_ok=True)

        count = len(os.listdir(location))

        data = ContentFile(base64.b64decode(imgstr), 'prediction.' + ext)

        fs = FileSystemStorage(location=location)
        filename = fs.save(data.name, data)
        file_url = location + '/' + filename

        model = load_model(settings.MODEL_PATH, compile=True)

        test_img = cv2.imread(file_url, cv2.IMREAD_UNCHANGED)
        # print(test_img)

        trans_mask = test_img[:, :, 3] == 0
        test_img[trans_mask] = [255, 255, 255, 255]

        new_img = cv2.cvtColor(test_img, cv2.COLOR_BGR2GRAY)

        test_img_resized = cv2.resize(new_img, (pic_size, pic_size))
        tst = test_img_resized.reshape(-1, pic_size, pic_size, 1)
        res = model.predict(tst)
        pred = np.argmax(res, axis=1)
        thisdict = {0: "A", 1: "a", 2: "B", 3: "b", 4: "C", 5: "Ç", 6: "D", 7: "d", 8: "E",
                    9: "e", 10: "ə", 11: "F", 12: "f", 13: "G", 14: "g", 15: "Ğ", 16: "ğ", 17: "H",
                    18: "h", 19: "X", 20: "ı", 21: "i", 22: "J", 23: "K", 24: "Q", 25: "q", 26: "L",
                    27: "l", 28: "M", 29: "m", 30: "N", 31: "n", 32: "O", 33: "ö", 34: "P", 35: "R",
                    36: "r", 37: "S", 38: "Ş", 39: "T", 40: "t", 41: "U", 42: "Ü", 43: "V", 44: "Y",
                    45: "Z"}

        return render(request, 'index.html', {
            'prediction': thisdict[pred[0]],
            # 'prediction': thisdict[10]
        })
    else:
        return render(request, 'index.html')
